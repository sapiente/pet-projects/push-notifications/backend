package cz.sapiente.pushnotifications.exception;

public class PushNotificationException extends RuntimeException {

    public PushNotificationException(final Throwable cause) {
        super(cause);
    }
}
