package cz.sapiente.pushnotifications.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.stereotype.Service;

import cz.sapiente.pushnotifications.exception.JsonSerializationException;
import lombok.NonNull;

@Service
public class JsonSerializer {

    private final ObjectMapper objectMapper;

    public JsonSerializer(@NonNull final ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    public String serialize(final Object object) {
        try {
            return this.objectMapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            throw new JsonSerializationException(e);
        }
    }
}
